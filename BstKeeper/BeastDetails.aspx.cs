﻿using BstKeeper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BstKeeper
{
    public partial class BeastDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public IQueryable<Beast> GetBeast([QueryString("BeastID")] int? beastID,
                        [RouteData] string beastName)
        {
            var _db = new BstKeeper.Models.BeastContext();

            IQueryable<Beast> query = _db.Beasts;

            if (beastID.HasValue && beastID > 0)
            {
                query = query.Where(p => p.BeastID == beastID);
            }
            else if (!String.IsNullOrEmpty(beastName))
            {
                query = query.Where(p =>
                          String.Compare(p.BeastName, beastName) == 0);
            }
            else
            {
                query = null;
            }
            return query;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BstKeeper.Models
{
    public class BeastMedical
    {

        [Key, ScaffoldColumn(false)]
        public int MedicalID { get; set; }

        [Required, StringLength(250), Display(Name = "Medical Facility")]
        public string MedicalFacility { get; set; }

        [Required, DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true), Display(Name = "Medical Visit Date")]
        public DateTime MedicalVisitDate { get; set; }

        [Required, StringLength(1000), Display(Name = "Medical Diagnosis")]
        public string MedicalDiagnosis { get; set; }

        [Required, StringLength(5000), Display(Name = "Medical Treatmen")]
        public string MedicalTreatment { get; set; }

        public int BeastID { get; set; }

        public virtual BeastCategory BeastCategory { get; set; }

    }
}
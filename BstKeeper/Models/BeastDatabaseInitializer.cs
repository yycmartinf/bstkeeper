﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BstKeeper.Models
{
    //Database to be recreated every time you ran the application
    //public class BeastDatabaseInitializer : DropCreateDatabaseAlways<BeastContext>

    public class BeastDatabaseInitializer : DropCreateDatabaseIfModelChanges<BeastContext>
    {
        protected override void Seed(BeastContext context)
        {
            //GetBeasts().ForEach(b => context.Beasts.Add(b));
            //GetBeastCategories().ForEach(c => context.BeastCategories.Add(c));
            //GetBeastMedicals().ForEach(bm => context.BeastMedicals.Add(bm));
            //GetBeastAppointments().ForEach(ba => context.BeastAppointments.Add(ba));

            var beastCategories = new List<BeastCategory>
            {
                new BeastCategory
                {
                    CategoryID = 1,
                    CategoryName = "Bird",
                    CategoryDescription = "A warm-blooded egg-laying vertebrate distinguished by the possession of feathers," +
                    " wings, and a beak and (typically) by being able to fly."
                },
                new BeastCategory
                {
                    CategoryID = 2,
                    CategoryName = "Mammal",
                    CategoryDescription = "A warm-blooded vertebrate animal of a class that is distinguished by the possession of hair or fur," +
                    " the secretion of milk by females for the nourishment of the young, and (typically) the birth of live young."

                },
                new BeastCategory
                {
                    CategoryID = 3,
                    CategoryName = "Insect",
                    CategoryDescription = "A small arthropod animal that has six legs and generally one or two pairs of wings."
                },
                new BeastCategory
                {
                    CategoryID = 4,
                    CategoryName = "Lizard",
                    CategoryDescription = "A reptile that typically has a long body and tail, four legs, movable eyelids, and a rough, scaly, or spiny skin."
                },
            };

            beastCategories.ForEach(bc => context.BeastCategories.Add(bc));
            context.SaveChanges();

            var beasts = new List<Beast>
            {
                new Beast
                {
                    BeastID = 1,
                    BeastName = "Bat",
                    BeastDescription = "The bat spirit animal is one of the most misunderstood animals because it has always been linked to darkness," +
                    " death, and the underworld. ... It may not look it, but the bat has a very sweet and warm nature. These creatures of the night are" +
                    " cute and cuddly, and they can even symbolize good luck and abundance.",
                    BeastImagePath ="bat.jpg",
                    CategoryID = 2
                },
                new Beast
                {
                    BeastID = 2,
                    BeastName = "Eagle",
                    BeastDescription = "Eagle conveys the powers and messages of the spirit; it is man's connection to the divine because it flies higher" +
                    " than any other bird. ... If eagle has appeared, it bestows freedom and courage to look ahead." +
                    " The eagle is symbolic of the importance of honesty and truthful principles.",
                    BeastImagePath ="eagle.jpg",
                    CategoryID = 1
                },
                new Beast
                {
                    BeastID = 3,
                    BeastName = "Snake",
                    BeastDescription = "The snake represents a life force as it is quite close to earth's energies. ... Snakes have always been symbols of opportunities" +
                    " and healing powers. Spiritual Guidance. The snake can also be a symbol of spiritual guidance and the presence of it in your" +
                    " life usually means you are in a transition period.",
                    BeastImagePath ="snake.jpg",
                    CategoryID = 4
                },
                new Beast
                {
                    BeastID = 4,
                    BeastName = "Butterfly",
                    BeastDescription = "Butterflies are deep and powerful representations of life. Many cultures associate the butterfly with our souls." +
                    " Around the world, people view the butterfly as representing endurance, change, hope, and life.",
                    BeastImagePath ="butterfly.jpg",
                    CategoryID = 3
                },

            };

            beasts.ForEach(b => context.Beasts.Add(b));
            context.SaveChanges();

            var beastMedicals = new List<BeastMedical>
            {
                new BeastMedical
                {
                    MedicalID = 1,
                    MedicalFacility = "Rocky View Vet Clinic",
                    MedicalVisitDate = Convert.ToDateTime("12/25/2008"),
                    MedicalDiagnosis ="Broken bone due to fall.",
                    MedicalTreatment = "Surgery.",
                    BeastID = 1
                },
                new BeastMedical
                {
                    MedicalID = 2,
                    MedicalFacility = "High River Vet Clinic",
                    MedicalVisitDate = Convert.ToDateTime("10/28/1998"),
                    MedicalDiagnosis ="Open wound on the right foot",
                    MedicalTreatment = "Provide stitches and antibiotics",
                    BeastID = 2
                },
                new BeastMedical
                {
                    MedicalID = 3,
                    MedicalFacility = "Calgary Vet Clinic",
                    MedicalVisitDate = Convert.ToDateTime("05/15/1985"),
                    MedicalDiagnosis ="Contusion on the tail",
                    MedicalTreatment = " Apply ice, compression, rest and  stop unnecessary physical activity. ",
                    BeastID = 3
                },
                new BeastMedical
                {
                    MedicalID = 4,
                    MedicalFacility = "Edmonton Vet Clinic",
                    MedicalVisitDate = Convert.ToDateTime("07/21/2010"),
                    MedicalDiagnosis ="Broken wing",
                    MedicalTreatment = "Pain medication, wing amputation. ",
                    BeastID = 4
                },
            };

            beastMedicals.ForEach(bm => context.BeastMedicals.Add(bm));
            context.SaveChanges();

            var beastAppointments = new List<BeastAppointment>
            {
                new BeastAppointment
                {
                    AppointmentID = 1,
                    AppoitmentFacility = "Rocky Vet Clinic",
                    AppointmentDate = Convert.ToDateTime("11/09/2019"),
                    AppointmentTime = Convert.ToDateTime("10:30"),
                    BeastID = 1
                },
                new BeastAppointment
                {
                    AppointmentID = 2,
                    AppoitmentFacility = "Brooks Vet Clinic",
                    AppointmentDate = Convert.ToDateTime("11/12/2019"),
                    AppointmentTime = Convert.ToDateTime("10:30"),
                    BeastID = 2
                },
                new BeastAppointment
                {
                    AppointmentID = 3,
                    AppoitmentFacility = "Ditsbury Vet Clinic",
                    AppointmentDate = Convert.ToDateTime("11/08/2019"),
                    AppointmentTime = Convert.ToDateTime("10:30"),
                    BeastID = 3

                },
                new BeastAppointment
                {
                    AppointmentID = 4,
                    AppoitmentFacility = "Gentle Vet Clinic",
                    AppointmentDate = Convert.ToDateTime("10/08/2019"),
                    AppointmentTime = Convert.ToDateTime("10:30"),
                    BeastID = 4
                },
            };

            beastAppointments.ForEach(ba => context.BeastAppointments.Add(ba));
            context.SaveChanges();

        }

    }
}
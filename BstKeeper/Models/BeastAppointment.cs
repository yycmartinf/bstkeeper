﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BstKeeper.Models
{
    public class BeastAppointment
    {
        [Key, ScaffoldColumn(false)]
        public int AppointmentID { get; set; }

        [Required, StringLength(250), Display(Name = "Appointment Facility")]
        public string AppoitmentFacility { get; set; }

        [Required, DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true), Display(Name = "Appointment Date")]
        public DateTime AppointmentDate { get; set; }

        [Required, DataType(DataType.Time), DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true), Display(Name = "Appointment Time")]
        public DateTime AppointmentTime { get; set; }

        public int BeastID { get; set; }

        public virtual BeastCategory BeastCategory { get; set; }



    }
}
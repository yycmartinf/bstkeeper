﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BstKeeper.Models
{
    public class Beast
    {
        [Key, ScaffoldColumn(false)]
        public int BeastID { get; set; }

        [Required, StringLength(75), Display(Name = "Beast Name")]
        public string BeastName { get; set; }

        [Required, StringLength(10000), Display(Name = "Beast Description"), DataType(DataType.MultilineText)]
        public string BeastDescription { get; set; }

        public string BeastImagePath { get; set; }

        public int CategoryID { get; set; }

        public virtual ICollection<BeastMedical> BeastMedicals { get; set; }

        public virtual ICollection<BeastAppointment> BeastAppointments { get; set; }

        public virtual BeastCategory BeastCategory { get; set; }

        

    }
}
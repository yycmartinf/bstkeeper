﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace BstKeeper.Models
{
    public class BeastContext : DbContext
    {
        public BeastContext() : base("BeastKeeper")
        {
        }

        public DbSet<Beast> Beasts { get; set; }
        public DbSet<BeastCategory> BeastCategories { get; set; }
        public DbSet<BeastMedical> BeastMedicals { get; set; }
        public DbSet<BeastAppointment> BeastAppointments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
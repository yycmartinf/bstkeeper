﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BstKeeper.Models
{
    public class BeastCategory
    {
        [Key, ScaffoldColumn(false)]
        public int CategoryID { get; set; }

        [Required, StringLength(50), Display(Name = "Beast Category Name")]
        public string CategoryName { get; set; }

        [Required, StringLength(1000), Display(Name = "Beast Category Description")]
        public string CategoryDescription { get; set; }

        public virtual ICollection<Beast> Beasts { get; set; }


    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BeastFinder.aspx.cs" Inherits="BstKeeper.BeastFinder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Slider -->
    <div class="container" style="width: 75%; height: 75%">
        <br />
        <br />
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <!-- Slider Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                <div class="item active">
                    <img src="\images\bat.jpg" alt="Bat" style="width: 100%">
                    <div class="carousel-caption">
                        <h3>

                            <a runat="server" href="~/Contact" style="color: white">Bat</a>
                        </h3>
                    </div>
                </div>

                <div class="item">
                    <img src="\images\eagle.jpg" alt="Eagle" style="width: 100%">
                    <div class="carousel-caption">
                        <h3>
                            <a runat="server" href="~/Contact" style="color: white">Eagle</a>
                        </h3>
                    </div>
                </div>

                <div class="item">
                    <img src="\images\snake.jpg" alt="Snake" style="width: 100%;">
                    <div class="carousel-caption">
                        <h3>
                            <a runat="server" href="~/Contact" style="color: white">Snake</a>
                        </h3>
                    </div>
                </div>

                <div class="item">
                    <img src="\images\butterfly.jpg" alt="Butterfly" style="width: 100%;">
                    <div class="carousel-caption">
                        <h3>
                            <a runat="server" href="~/Contact" style="color: white">Butterfly</a>
                        </h3>
                    </div>
                </div>


                <!-- Slider Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</asp:Content>

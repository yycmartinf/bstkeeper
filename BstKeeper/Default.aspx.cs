﻿using BstKeeper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BstKeeper
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (BeastContext _db = new BeastContext())
            {

                // Access the database
                // Determine which record in the database we are supposed show by looking at the beastID
                string recordToFind = Request.QueryString["BeastID"] ?? "1";
                int beastToFind = Convert.ToInt32(recordToFind);

                // Retrieve that record from the database
                var queryResults = from Beasts in _db.Beasts
                                   where Beasts.BeastID.Equals(beastToFind)
                                   select Beasts;

                // Get the first record from the result, or the default
                Beast foundBeast = queryResults.FirstOrDefault<Beast>();

            }

            if (!Page.IsPostBack)
            {

            }
            else
            {
                // Post back

            }
        }
    }
}
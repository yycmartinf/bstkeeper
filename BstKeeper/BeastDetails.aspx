﻿<%@ Page Title="Beast Details" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BeastDetails.aspx.cs" Inherits="BstKeeper.BeastDetails" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
  
    <asp:FormView ID="BeastView" runat="server" ItemType="BstKeeper.Models.Beast" SelectMethod="GetBeast" RenderOuterTable="false">

      <ItemTemplate>
            <div>
                <h1>Beast Details for <%#Item.BeastName %></h1>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <img src="/Images/<%#:Item.BeastImagePath %>" style="border: solid; height: 300px" alt="<%#:Item.BeastName %>" />
                    </td>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top; text-align: left;">
                        <b>Beast ID:</b><br /><%#:Item.BeastID %><br />
                        <b>Beast Name:</b><br /><%#:Item.BeastName%><br />
                        <b>Beast Description:</b><br /><%#:Item.BeastDescription %><br />
                        <b>Beast Category:</b><br /><%#:Item.BeastCategory.CategoryName %><br />
                        <b>Beast Medical:</b><br /><%#:Item.BeastMedicals.Count%><br />
                        <b>Beast Appointment:</b><br /><%#:Item.BeastAppointments.Count%><br />
                    </td>
                </tr>
            </table>
        </ItemTemplate>


    </asp:FormView>

</asp:Content>
